package kz.aitu.oop;


public class Square extends Rectangle {
    public Square() {
        super(1.0, 1.0);
    }

    public Square(double side) {
        super(side,side);
    }

    public Square(double side, String col, boolean fill) {
        super(side, side, col, fill);
    }
    public double getSide()
    {
        return super.getWidth();
    }

    public void setSide(double side)
    {
        super.setLength();
        super.setWidth();
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
        super.setWidth(side);
    }
    @Override
    public String toString(){
        return "A Square with side="+getLength();
    }

}
