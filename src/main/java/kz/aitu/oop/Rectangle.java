package kz.aitu.oop;

public class Rectangle extends Shape {
    private double width;
    private double length;

    public Rectangle() {
        this.width = 1.0;
        this.length = 1.0;
    }
    public Rectangle(double w, double l) {
        this.width = w;
        this.length = l;
    }
    public Rectangle(double w, double l, String col, boolean fill) {
        this.width = w;
        this.length = l;
    }
    public double getWidth(){
        return width;
    }
    public void setWidth(double width){
        this.width=width;
    }
    public double getLength(){
        return length;
    }
    public void setLength(double length){
        this.length = length;
    }
    public double getArea(){
        return this.width * this.length;
    }
    public double getPerimeter() {
        return (this.length + this.length) * 2;
    }
    @Override
    public String toString(){
        return "A Circle with width="+this.width+" and length="+this.length;
    }
}
