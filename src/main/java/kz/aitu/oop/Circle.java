package kz.aitu.oop;

public class Circle extends Shape {
      private double radius;

      public Circle(){
          this.radius = 1.0;
      }
      public Circle (double r){
          this.radius = r;
      }
      public Circle (double r, String col,boolean fill){
          this.radius = r;
      }
      public double getRadius(){
          return radius;
      }
      public void setRadius(double radius){
          this.radius = radius;
      }
      public double getArea(){
          double pi = 3.14;
          double area = pi * this.radius * this.radius;
          return area;
      }
      public double getPerimeter(){
          return (this.radius * 2) * 3.14;
      }
      @Override
        public String toString(){
          return "A Circle with radius="+this.radius;
      }
}
