package kz.aitu.oop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableCaching
//@EnableJpaRepositories
//@SpringBootApplication
public class OopApplication {
	public static void main(String[] args) {
		SpringApplication.run(OopApplication.class, args);
	}
}
