package kz.aitu.oop;

public class point {

    public static void main(String[] args) {
        point point1 = new point(3,5);
        point point2 = new point(2,8);
        System.out.println(point2.dist(point1));

    }
    public double x;
    public double y;

    public point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double x() {
        return x;
    }

    public double y() {
        return y;
    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }

    public void setX ( double x)
    {
        this.x = x;

        }
            public void setY ( double y){
        this.y = y;
    }
    public double dist(point point) {
        double dist = Math.sqrt(
                Math.pow(point.getX() - this.getX(), 2) + Math.pow(point.getY() - this.getY(), 2)
        );
        return dist;
    }

}

