package kz.aitu.oop.examples.train;

public class Train {
    public int idTrain;
    public String name;
    private String from;
    private String where;
    private int seats;
    private int booked;
    private int numPassenger;

    public Train(){

    }
    public Train(int idTrain,String name, String from, String where, int seats, int booked, int numPassenger){
        this.idTrain = idTrain;
        this.name = name;
        this.from = from;
        this.where = where;
        this.seats = seats;
        this.booked = booked;
        this.numPassenger = numPassenger;
    }

    public void setNumPassenger(int NumPassenger){
        this.numPassenger = numPassenger;
    }
    public int  getNumPassenger(){
        return numPassenger;
    }
    public String getFrom(){
        return from;
    }
    public void setFrom(String from){
        this.from = from;
    }
    public void addBooked(int newBooked){
       // booked.(newBooked);
    }
}
