package kz.aitu.oop.examples.train;

import java.util.Date;

public class TrainStation {
    public String station;
    public int id;
    public Date time;

    public TrainStation(){

    }
    public TrainStation(String station, int id, Date time){
        this.id = id;
        this.station = station;
        this.time = time;
    }
    public String getStationName(){
        return station;
    }
    public void setStationName(String station){
        this.station = station;
    }
    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id = id;
    }
    @Override
    public String toString(){
        return "The Train" + id + "arrives at " + station + "station.";
    }
}
