package kz.aitu.oop.examples.jdbs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Task2 {
    public boolean fileExists (String string){
        try {
            for(String line : Files.readAllLines(Paths.get("file name"))){
                String [] data = line.split(",");{
                    for(int i = 0; i < data.length;i++){
                        return true;
                    }
                }
            }
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        return false;
    }
    public static boolean isInt(String string) {
        try {
            Integer.parseInt(string);
            return true;
        }
        catch (java.lang.Exception ex) {
            return false;
        }
    }
        public static boolean isDouble(String string){
        try{
            Double.parseDouble(string);
            return true;
        }
        catch(java.lang.Exception ex){
            return false;
        }
    }
}
