package kz.aitu.oop.examples.jdbs;

public class Main{
    public static void main(String[]args){
        Task2 task = new Task2();
        System.out.println(Task2.isInt("Almaty"));
        System.out.println(Task2.isDouble("book"));
        System.out.println(Task2.isInt("1"));
        System.out.println(Task2.isDouble("eleven"));
        System.out.println(Task2.isDouble("1.24"));
        System.out.println(Task2.isDouble("Astana"));
    }
}
