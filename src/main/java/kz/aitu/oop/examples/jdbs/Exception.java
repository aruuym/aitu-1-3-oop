package kz.aitu.oop.examples.jdbs;

import java.io.FileNotFoundException;

public class Exception {
    public Exception(String file) {
        file = "An exception in main";
        System.out.println(file);
    }
    public static void main(String args[] ) {
        try {
            throw new FileNotFoundException();
        }
         /*catch (java.lang.Exception ex){
            throw new Exception("file");
         }*/
        catch(java.lang.Exception ex) {
            System.out.println("ex.getMessage() = "  + ex.getMessage());
        }
        finally{
            System.out.println("completed");
        }
    }
}
