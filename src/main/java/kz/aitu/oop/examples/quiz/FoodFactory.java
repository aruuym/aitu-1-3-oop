package kz.aitu.oop.examples.quiz;

public class FoodFactory {
    Cake cake = new Cake();
    Pizza pizza = new Pizza();

    public void getFood(String food){
        if(food == "cake")
            cake.getType();
        if (food == "pizza")
            pizza.getType();
        if(food != "cake" && food != "pizza")
            System.out.println("Error");
    }
}
