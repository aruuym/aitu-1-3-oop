package kz.aitu.oop.examples.quiz;

public class Cake implements Food {
    @Override
    public void getType() {
        System.out.println("The factory returned class Cake");
        System.out.println("Someone ordered Dessert");
    }
}
