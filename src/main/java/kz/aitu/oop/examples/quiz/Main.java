package kz.aitu.oop.examples.quiz;

public class Main {
    public static void main(String[] args) {
        FoodFactory foodFactory = new FoodFactory();
        foodFactory.getFood("cake");
        foodFactory.getFood("pizza");
        foodFactory.getFood("bread");
    }
}
