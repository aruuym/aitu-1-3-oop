package kz.aitu.oop.examples.company;

import lombok.Data;

@Data
public class Company {
    private static int id;
    private static String name;
    public String department;

    public Company(){

    }
    public Company(String name, int id, String department){
        this.name= name;
        this.id = id;
        this.department = department;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "id ->" + id + "\n" +
                "Company name ->" + name + "\n" +
                "department ->" + department;
    }

}
