package kz.aitu.oop.examples.company;

public class Collected extends Employee {
    public double cost;
    public double skill;

   public Collected(){

   }
   public Collected(double cost, double skill, String name){
       this.cost = cost;
       this.skill = skill;
       this.name = name;
   }
   public double getCost(){
       return cost;
   }
   public void setCost(double cost){
       this.cost=cost;
   }
   public String getName(){
       return name;
   }
   public void setName(String name){
       this.name=name;
   }
   public double getSkill(){
       return skill;
   }
    public String toString(){
        return ("Employee's Name is "+name+ "  salary: $"+ this.getCost());
    }
}
