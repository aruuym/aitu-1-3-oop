package kz.aitu.oop.examples.company;

public class ComMain {

    public static void main(String[] args) {
        Company company = new Company();
        company.setName("Microsoft");
        System.out.println(company);
        Collected collected = new Collected();
        collected.setName("Aruzhan");
        collected.setCost(150);
        System.out.println(collected);
    }
}
