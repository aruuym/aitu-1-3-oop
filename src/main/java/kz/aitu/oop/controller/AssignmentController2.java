package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
//import kz.aitu.oop.examples.Point;
import kz.aitu.oop.repository.StudentFileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/api/task/2")
public class AssignmentController2 {


    @GetMapping("/students")
    public ResponseEntity<?> getStudents() throws FileNotFoundException {

        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        for (Student student: studentFileRepository.getStudents()) {
            result += student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "</br>";
        }

        return ResponseEntity.ok(result);
    }

    /**
     * Method get all Students from file and calculate average name length
     * @return average name length of the all students
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsNameLength")
    public ResponseEntity<?> averageStudentsNameLength() throws FileNotFoundException {

        double average = 0;

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: StudentFileRepository.getStudents()) {
            total += student.getName().length();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }


    @GetMapping("/averageStudentsCount")
    public ResponseEntity<?> averageStudentsCount() throws FileNotFoundException {

        double count = 0;
        double average = 0;
        int n= 0;
        for (Student student: StudentFileRepository.getStudents()) {
            count+=student.getName().length();
            n++;
        }
        average = count / n;
        return ResponseEntity.ok(average);
    }

    @GetMapping("/averageStudentsPoint")
    public ResponseEntity<?> averageStudentsPoint() throws FileNotFoundException {

        double average = 0;
        double total = 0;
        double count = 0;
        for (Student student : StudentFileRepository.getStudents()) {
            total += student.getPoint();
            count++;
        }
        average = total / count;
            return ResponseEntity.ok(average);

    }
    @GetMapping("/averageStudentsAge")
    public ResponseEntity<?> averageStudentsAge() throws FileNotFoundException {

        double average = 0;
        int count = 0;
        double total= 0;
        for (Student student: StudentFileRepository.getStudents()) {
            total += student.getAge();
            count++;
        }
        average = total / count;
        return ResponseEntity.ok(average);
    }

    @GetMapping("/highestStudentsPoint")
    public ResponseEntity<?> highestStudentsPoint() throws FileNotFoundException {

        double maxPoint = 0;
        for (Student student: StudentFileRepository.getStudents()) {
            if(maxPoint <  student.getPoint()){
                maxPoint = student.getPoint();
            }
        }
        return ResponseEntity.ok(maxPoint);
    }

    @GetMapping("/highestStudentsAge")
    public ResponseEntity<?> highestStudentsAge() throws FileNotFoundException {

        double maxAge = 0;
        for (Student student: StudentFileRepository.getStudents()) {
            if(maxAge <  student.getAge()){
                maxAge = student.getAge();
            }
        }

        return ResponseEntity.ok(maxAge);
    }

    @GetMapping("/highestGroupAveragePoint")
    public ResponseEntity<?> highestGroupAveragePoint() throws FileNotFoundException {

        double averageGroupPoint = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        Hashtable<String, List<Student>> groups = new Hashtable<>();
        for (Student student: StudentFileRepository.getStudents()) {
            if(groups.containsKey(student.getGroup())){
                groups.put(student.getGroup(), new ArrayList<>());
            }
            groups.get(student.getGroup()).add(student);
        }
                double maxAverageGroupPoint = 0;
        for (List<Student> studentList: groups.values()){
            double total  = 0;
            for (Student student:studentList){
                total +=student.getPoint();
            }
            double averageGroup = total / studentList.size();
            System.out.println(averageGroup);
            if(averageGroup  >  maxAverageGroupPoint) maxAverageGroupPoint = averageGroup;
        }
        return ResponseEntity.ok(averageGroupPoint);
    }

    @GetMapping("/highestGroupAverageAge")
    public ResponseEntity<?> highestGroupAverageAge() throws FileNotFoundException {

        double averageGroupAge = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        Hashtable<String,List<Student>> groups = new Hashtable<>();
        for (Student student: StudentFileRepository.getStudents()) {
            if(groups.containsKey(student.getGroup())){
                groups.put(student.getGroup(), new ArrayList<>());
            }
            groups.get(student.getGroup()).add(student);
        }
        double maxAverageGroupAge = 0;
        for (List<Student> studentList: groups.values()){
            double total  = 0;
            for (Student student:studentList){
                total +=student.getAge();
            }
            double averageGroup = total / studentList.size();
            System.out.println(averageGroup);
            if(averageGroup  >  maxAverageGroupAge) maxAverageGroupAge = averageGroup;
        }
        return ResponseEntity.ok(averageGroupAge);
    }


    @GetMapping("/all")
    public ResponseEntity<?> allData() throws FileNotFoundException {

        String result = "";
        result += "averageStudentsNameLength: " + averageStudentsNameLength().getBody() + "</br>";
        result += "averageStudentsCount: " + averageStudentsCount().getBody() + "</br>";
        result += "averageStudentsPoint: " + averageStudentsPoint().getBody() + "</br>";
        result += "averageStudentsAge: " + averageStudentsAge().getBody() + "</br>";
        result += "highestStudentsPoint: " + highestStudentsPoint().getBody() + "</br>";
        result += "highestStudentsAge: " + highestStudentsAge().getBody() + "</br>";
        result += "highestGroupAveragePoint: " + highestGroupAveragePoint().getBody() + "</br>";
        result += "highestGroupAverageAge: " + highestGroupAverageAge().getBody() + "</br>";

        return ResponseEntity.ok(result);
    }
}
