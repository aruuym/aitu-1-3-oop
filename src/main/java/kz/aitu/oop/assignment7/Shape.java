package kz.aitu.oop.assignment7;

public abstract class Shape {

    private String color;
    private boolean filled;

    public Shape() {
        this.color = "red";
        this.filled = true;
    }

    public Shape(String col, boolean fill) {
        this.color = col;
        this.filled = fill;
    } public String getColor() {
        return color;
    }
    public boolean isFilled() {
        return filled;
    }

    public void setColor (String color) {
        this.color = color;
    }
    public void setFilled (boolean filled){
        this.filled = filled;
    }
    public String filled() {
        if (this.filled) {
            return "filled";
        } else {
            return "no filled";
        }
    }
    abstract public double getArea();

    abstract public double getPerimeter();

    public String toString(){
        return "A Shape with color of "+color+" and "+filled();
    }
}