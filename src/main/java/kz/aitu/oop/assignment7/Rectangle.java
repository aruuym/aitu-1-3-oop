package kz.aitu.oop.assignment7;

public class Rectangle extends Shape {
        private double width;
        private double length;

        public Rectangle() {
            this.width = 1.0;
            this.length = 1.0;
        }

        public Rectangle(double wid, double leng) {
            this.width = wid;
            this.length = leng;
        }

        public Rectangle(double wid, double leng, String col, boolean fill) {
            super(col, fill);
            this.width = wid;
            this.length = leng;
        }
        public double getWidth(){
            return width;
        }
        public void setWidth(double width){
            this.width=width;
        }
        public double getLength(){
            return length;
        }
        public void setLength(double length){
            this.length = length;
        }

         public double getArea(){
            return this.width * this.length;
        }

         public double getPerimeter() {
             return (this.length + this.length) * 2;
        }
        @Override
        public String toString(){
            return "A Width="+this.width+" and length="+this.length+", subclass of"+super.toString();
        }

    }

