package kz.aitu.oop.assignment7;

import lombok.Data;

@Data
public class MovablePoint implements Movable {
    private int x;
    private int y;
    private int xSpeed;
    private int ySpeed;

    public  MovablePoint(int xx,int yy,int xSpeed,int ySpeed){
        this.x = xx;
        this.y = yy;
        this.xSpeed = xSpeed;
        this.xSpeed = ySpeed;
    }


    @Override
    public void moveUp() {
        y++;
    }

    @Override
    public void moveDown() {
        y--;
    }

    @Override
    public void moveLeft() {
        x--;
    }

    @Override
    public void moveRight() {
        x++;
    }
}
