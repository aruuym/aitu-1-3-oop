package kz.aitu.oop.assignment7;

import lombok.Data;

@Data
public class MovableCircle implements Movable {
    private int radius;
    private MovablePoint center;

    public MovableCircle(int xx , int yy, int xSpeed, int ySpeed,int radius) {
        center.x(xx);
        center.y(yy);
        center.xSpeed(xSpeed);
        center.ySpeed(ySpeed);
        this.radius = radius;
    }

    @Override
    public void moveUp() {
        center.y(center.gety()+1);
    }

    @Override
    public void moveDown() {
        center.y(center.gety()-1);
    }

    @Override
    public void moveLeft() {
        center.x(center.getx()-1);
    }

    @Override
    public void moveRight() {
        center.x(center.getx()+1);
    }
    public String toString(){
        return "x= " + center.getx() + " - y = " + center.gety() + " , speed of x is " + center.getxSpeed() + " , speed of y is " + center.getySpeed() + ", radius is " + this.radius;
    }
}
