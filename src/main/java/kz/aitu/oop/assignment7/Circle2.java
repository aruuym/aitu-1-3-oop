package kz.aitu.oop.assignment7;

public class Circle2<TestCircle> implements GeometricObject {
    private double radius;

    public Circle2(){
        this.radius = 1.0;
    }
    public Circle2(double radius){
        this.radius = radius;
    }
    @Override
    public double getArea(){
        double pi = 3.14;
        double area = pi * this.radius * this.radius;
        return area;
    }
    public double getPerimeter(){
        return (this.radius * 2) * 3.14;
    }
    public String toString(){
        return "radius = " + this.radius;
    }
    public double TestCircle{
        public static void main(String args[]){
            Circle c1 = new Circle();
            Circle c2 = new Circle (10.0);

            c1.getPerimeter();
        }
    }
}
