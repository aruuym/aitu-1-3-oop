package kz.aitu.oop.assignment7;

public class Square extends Rectangle{
    private double side;

    public Square(){
        super(1.0,1.0);
    }
    public Square (double sid){
        super(sid,sid);
        this.side = sid;
    }
    public Square(double sid,String col,boolean fill){
        super(sid,sid,col,fill);
        this.side = sid;
    }
    public double getSide(){
        return side;
    }
    public void setSide(double side){
        this.side = side;
    }

    @Override
    public void setLength(double sid) {
        super.setLength(sid);
        super.setWidth(sid);
    }

    public void setWidth(double sid){
        super.setLength(sid);
        super.setWidth(sid);
        this.side=sid;
    }

    @Override
    public String toString() {
        return "Side =" +getLength()+",subclass" + super.toString();
    }
}
