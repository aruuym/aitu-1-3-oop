package kz.aitu.oop;

public class Shape {

          private String color;
          private boolean filled;

          public Shape(){
              this.color = "green"
              this.filled = "true"
          }

          public Shape(String col,boolean fill) {
             this.color = col;
             this.filled = fill;
          }
          public String getColor() {
              return color;
          }
            public boolean isFilled() {
              return filled;
          }

          public void setColor (String color) {
          this.color = color;
            }
          public void setFilled (boolean filled){
            this.filled = filled;
             }
          public String filled(){
              if(this.filled){
                  return "filled";
              }
              else {
                  return "not filled";
              }
          }
          public String toString(){
              return "A Shape with color of"+color+"and"+filled();
          }
  }
